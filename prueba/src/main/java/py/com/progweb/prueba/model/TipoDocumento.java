package py.com.progweb.prueba.model;

import javax.persistence.*;

@Entity
@Table(name = "tipo_documento")
public class TipoDocumento {
	
    @Id
	@GeneratedValue(generator = "tipo_documentoSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tipo_documentoSec", sequenceName = "tipo_documento_id_seq", allocationSize = 0)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descripcion", length = 100)
    @Basic(optional = false)
    private String descripcion;

    public TipoDocumento(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
