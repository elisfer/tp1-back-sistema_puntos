package py.com.progweb.prueba.model;

import javax.persistence.*;

@Entity
@Table(name = "nacionalidad")
public class Nacionalidad {
	
    @Id
	@GeneratedValue(generator = "nacionalidadSec", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "nacionalidadSec", sequenceName = "nacionalidad_id_seq", allocationSize = 0)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "descripcion", length = 100)
    @Basic(optional = false)
    private String descripcion;

    public Nacionalidad(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
